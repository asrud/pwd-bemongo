const express = require("express");
const body = require("body-parser");
const cors = require("cors");
const { MongoClient, ObjectId } = require("mongodb");

const PORT = 2000;
const app = express();

app.use(body());
app.use(cors());

// setup mongodb
let url =
  "mongodb+srv://asrud:12345@cluster0.gt9x1.mongodb.net/db_kantor?retryWrites=true&w=majority";
MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
  if (err) {
    console.log(err);
  }
  console.log("Connected to MongoDB Cloud");
});

// post data mongodb
app.post("/add-data", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan").insertMany(
      [{ nama: "Budi", report: "Laporan hasil kerja" }],
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send(err);
        }
        res.status(200).send(results);
      }
    );
  });
});

// get all data mongodb
app.get("/get-data", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan")
      .find({})
      .toArray((err, docs) => {
        if (err) res.status(500).send(err);
        res.status(200).send(docs);
      });
  });
});

// limit dan sorting
app.get("/get-sortlimit", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan")
      .find({})
      // sort asc
      // .sort({ usia: 1 })
      // sort desc
      // .sort({ usia: -1 })
      // limit pagination
      .limit(5)
      .skip(2)
      .toArray((err, docs) => {
        if (err) res.status(500).send(err);
        res.status(200).send(docs);
      });
  });
});

// filter data mongodb
app.get("/get-filter", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan")
      // .find({...req.query})
      // .find({ $and: [{ nama: "rudi" }, { usia: 26 }] })
      // .find({ $or: [{ nama: "rudi" }, { nama: "Budi" }] })
      // .find({ usia: { $gt: 25 } })
      .find({ usia: { $lt: 25 } })
      .toArray((err, docs) => {
        if (err) res.status(500).send(err);
        res.status(200).send(docs);
      });
  });
});

// group data mongodb
app.get("/get-group", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan")
      .aggregate([
        {
          $group: {
            _id: "$kota",
            avgUsia: { $avg: "$usia" },
            count: { $sum: 1 },
          },
        },
      ])
      .toArray((err, docs) => {
        if (err) res.status(500).send(err);
        res.status(200).send(docs);
      });
  });
});

// edit data
app.patch("/update", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan").updateOne(
      { ...req.query },
      { $set: { ...req.body } },
      (err, results) => {
        if (err) {
          console.log(err);
          res.status(500).send(err);
        }
        res.status(200).send(results);
      }
    );
  });
});

// delet data
app.delete("/delete", (req, res) => {
  MongoClient.connect(url, { useUnifiedTopology: true }, (err, client) => {
    const db = client.db("db_kantor");
    db.collection("karyawan").deleteOne({ ...req.query }, (err, results) => {
      if (err) {
        console.log(err);
        res.status(500).send(err);
      }
      res.status(200).send(results);
    });
  });
});

app.listen(PORT, () => console.log("server running at port : ", PORT));
